 README

Instalação
1º Instale o Ruby: https://rubyinstaller.org/


2º Atualize a Gems:  http://rubyforge.org/frs/?group_id=126


Abra o prompt de comando do MS-DOS e acesse a pasta onde você descompactou o rubygems. Dentro dela existe um arquivo chamado setup.rb.No prompt de comando digite:

ruby setup.rb


No cmd digite: gem install rails --version “1.2.6”

Gerando primeiro projeto: rails new contatos

Caminhe até a pasta gerada e rode o seguinte comando: Bundle install (se caso as gems ainda estiverem desatualizadas)

Criando um CRUD: rails generate scaffold agenda nome:string telefone:string

Fazendo a migração para o banco de dados: rails db:migrete

Subindo o servidor: rails server

Endereço para acesso: localhost:3000

Video explicativo: https://www.youtube.com/watch?v=wLbxo5EpFTQ&t=10s

